import React, {Component} from 'react';
import TodoService from "../../services/todo.services";
import UserService from "../../services/user.services";
import {Link} from 'react-router-dom';


export default class TodoDetails extends Component{

    constructor(props) {
        super(props);
        this.state = {
            todo: {completed : ""},
            userName : ""
        }
    }

    async componentDidMount() {
        let response = await TodoService.details(this.props.match.params.id);
        this.setState({todo: response.data});
        let response2 = await UserService.details(this.state.todo.userId);
        this.setState({userName: response2.data.username});
        console.log(this.state)
    }

    async handleDelete(id){
        await TodoService.delete(id);
        this.props.history.push('/todo');
    }

    render() {
        let {todo, userName} = this.state;
        return <div className="container">
            <h1><Link to={`/user/${todo.userId}/details`} className="btn btn-primary">{todo.title} </Link></h1>
            <h3>créer par : {userName}</h3>
            <p>complété : {todo.completed.toString()}</p>
            <Link to={`/todo/${todo.id}/update`} className="btn btn-primary">Modifier</Link>
            <button className="btn btn-danger" onClick={() => this.handleDelete(todo.id)}>Supprimer</button>
        </div>
    }

}
