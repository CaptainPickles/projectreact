import React, {Component} from 'react';
import Todo from "../../components/todo";
import TodoService from "../../services/todo.services";

export default class TodoList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            todos: []
        };
    }

    async componentDidMount() {
         let todos = await TodoService.list(5);
         this.setState({todos: todos});
    }

    render() {
        let {todos} = this.state;
        return <div className="container">
            <h1>Listes des todos</h1>
            <div className="row">

                {todos.length === 0 && <p>Aucun todo pour le moment...</p>}

                {todos.length > 0 && todos.map(todo => {
                    return <div className="col-md-4">
                        <Todo Todo={todo}/>
                    </div>
                })}

            </div>

        </div>
    }
}
