import React, {Component} from 'react';
import TodoService from "../../services/todo.services";

export default class TodoUpdate extends Component{

    constructor(props) {
        super(props);
        this.state = {
            todo: {},
            title: "",
            completed: ""
        }
    }

    async componentDidMount() {
        let {id} = this.props.match.params;
        let response = await TodoService.details(id);
        this.setState({
            todo: response.data,
            title: response.data.title,
            completed: response.data.completed
        });
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async handleSubmit(e){
        e.preventDefault();
        let {todo, title, completed} = this.state;

        let data = {
            title: title,
            completed: completed
        }

        await TodoService.update(todo.id, data);
        this.props.history.push(`/todo/${todo.id}/details`);
    }

    render() {
        let {todo, title, completed} = this.state;
        return <div className="container">
            <h1>Modification d un todo</h1>
            <h2>{todo.title}</h2>
            <form onSubmit={(e) => this.handleSubmit(e)}>
                <div className="form-group">
                    <label>Titre</label>
                    <input type="text" className="form-control" id="title" required value={title} onChange={(e) => this.handleChange(e)}/>
                </div>
                <div className="form-group">
                    <label>complété</label>
                    <select className="form-select" id="completed" onChange={(e) => this.handleChange(e)}>
  <option value="false">non Terminé</option>
  <option value="true">Terminé</option>
</select>
                </div>
                <button type="submit" className="btn btn-primary">Modifier</button>
            </form>


        </div>
    }
}
