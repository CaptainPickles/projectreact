import React, {Component} from 'react';
import UserService from "../../services/user.services";

export default class usersList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            users: []
        };
    }

    async componentDidMount() {
         let users = await UserService.list();
         this.setState({users: users});
    }

    render() {
        let {users} = this.state;
        return <div className="container">
            <h1>Listes des todos</h1>
            <div className="row">

                {users.length === 0 && <p>Aucun users pour le moment...</p>}

                {users.length > 0 && users.map(user => {
                    return <div className="col-md-4">
                        <p>J'allais faire un tableau pour chaque user</p>
                    </div>
                })}

            </div>

        </div>
    }
}
